# To add a new cell, type '#%%'
# To add a new markdown cell, type '#%% [markdown]'

#%%
import spacy
from spacy import displacy
import time
import pandas as pd
import spacy_udpipe
import io


#%%
nlp = spacy.load("en_core_web_sm")


#%%
text = u"Wikipedia is a free online encyclopedia, created and edited by volunteers around the world."


#%%
doc = nlp(text)


#%%
data=[[token.text, token.lemma_, token.pos_, token.tag_, token.dep_, token.shape_, token.is_alpha, token.is_stop] for token in doc]


#%%
pd.DataFrame(data, columns=["text", "lemma","pos", "tag", "dep","shape", "is_alpha", "is_stop"])


#%%
sentence_spans = list(doc.sents)


#%%
displacy.render(sentence_spans, style="dep")


#%%
spacy_udpipe.download("en")  # download English model
nlp = spacy_udpipe.load("en")


#%%
doc = nlp(text)


#%%
data=[[token.text, token.lemma_, token.pos_, token.tag_, token.dep_, token.shape_, token.is_alpha, token.is_stop] for token in doc]


#%%
pd.DataFrame(data, columns=["text", "lemma","pos", "tag", "dep","shape", "is_alpha", "is_stop"])


#%%
sentence_spans = list(doc.sents)


#%%
displacy.render(sentence_spans, style="dep")


#%%
nlp_spacy = spacy.load("en_core_web_sm")


#%%
nlp_udpipe = spacy_udpipe.load("en")


#%%
text=""


#%%
import urllib.request


#%%
req = urllib.request.Request('https://archive.org/stream/HarryPotterAndDeathlyHallows/harry%20potter%20and%20deathly%20hallows_djvu.txt')
response = urllib.request.urlopen(req)
the_page = response.read()


#%%
text = the_page.decode("utf-8")[:1000000]


#%%
t_spacy1 = time.perf_counter()


#%%
doc_spacy = nlp_spacy(text)


#%%
t_spacy2 = time.perf_counter()


#%%
t_udpipe1 = time.perf_counter()


#%%
doc_udpipe = nlp_udpipe(text)


#%%
t_udpipe2 = time.perf_counter()


#%%
t_udpipe2-t_udpipe1


#%%
t_spacy2-t_spacy1
