print('prepare libs...')
import time
import spacy
import spacy_udpipe
nlp_spacy = spacy.load("en_core_web_sm")  # load model package "en_core_web_sm"
spacy_udpipe.download("en")  # download English model
nlp_spacy_udpipe = spacy_udpipe.load("en")
print('prepared libs')
print('prepare text...')
text = u"Wikipedia is a free online encyclopedia, created and edited by volunteers around the world."
print('prepared text')
print('working spacy...')
print('The time is:', time.time())
doc_spacy = nlp_spacy(text)
doc_spacy_udpipe = nlp_spacy_udpipe(text)
print('The time is:', time.time())

tokens_spacy = [token for token in doc_spacy]
tokens_spacy_udpipe = [token for token in doc_spacy_udpipe]

for token in doc_spacy:
    print(token.text, token.lemma_, token.pos_, token.dep_, token.head)
for token in doc_spacy_udpipe:
    print(token.text, token.lemma_, token.pos_, token.dep_)
